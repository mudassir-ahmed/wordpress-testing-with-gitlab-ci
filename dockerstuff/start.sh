#!/bin/bash

#set -v

echo "This requires you to have a database.sql file in the desired location"

# Start the containers in the background
docker-compose up -d

# Wait for the containers to actually run
# If you get error: `read unix @->/var/run/docker.sock: read: connection reset by peer` - this could be possibly due to the containers needing more time to get things ready above
# Bit hacky...
#sleep 30

# SSH into the mysql container
# Notice why we append _db_1 to repository to generate the container name repository_db_1
# AND..
# Let's import the database which should be located in the current directory as wordpress.sql
# We have access to wordpress.sql in this container due to the fact we created a volume in docker-compose.yml
# Notice how the username, password and database in named wordpress.
MYSQL_ROOT_PASSWORD='somewordpress'


# Mentioned under 'No connections until MySQL init completes' on https://hub.docker.com/_/mysql
# Implementation inspired from https://stackoverflow.com/questions/7252236/bash-check-mysql-connect
# Keep looping until we can establish a mysql connection and supress errors from stderr
until docker exec -i dockerstuff_mysql-database_1 sh -c "mysql -u root -p$MYSQL_ROOT_PASSWORD -e ';'" 2> /dev/null; do
    echo "Can't connect, retrying..."
    sleep 2
done

echo "Importing wordpress database"
docker exec -i dockerstuff_mysql-database_1 sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" wordpress' < ../repository/assets/database.sql

# set -x
# Make sure URLS in wordpress database match to localhost:8000 - as set out in docker-compose.yml


# Run the SQL query
# Notice how most of the mysql command line options are environment variables in docker-compose.yml
echo "Changing URLs in wordpress database"
docker exec -i dockerstuff_mysql-database_1 sh -c "mysql --user=wordpress --password=wordpress --database=wordpress --execute=\"UPDATE wp_options SET option_value='http://localhost:8000' WHERE option_name='siteurl' OR option_name='home'\";";

# Some useful SQL commands...
# SELECT * FROM wp_options WHERE option_name="siteurl" OR name="home";
# UPDATE wp_options SET option_value="http://localhost:8000" WHERE option_name="siteurl" OR option_name="home";

# References:
#   http://phase2.github.io/devtools/common-tasks/ssh-into-a-container/
