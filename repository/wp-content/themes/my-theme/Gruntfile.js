module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
       
        /* WARNING: no css prefixer set up */

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
            },
            modules: {
                files: {
                    'assets/js/modules.min.js' : [
                        // Path to module scripts go here... like jQuery.
                        // However, jQuery is included with WordPress.
                    ]
                },
            },
            scripts: {
                files: {
                    // destination : source(s)
                    'assets/js/scripts.min.js': 'assets/js/src/*.js',
                }
            },
        },

        sass: {
            dist: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed',
                },
                files: {
                    'assets/css/main.css': 'assets/css/sass/main.scss',
                },
            },
        },

        watch: {
            css: {
                files: ['assets/css/sass/*.scss'],
                tasks: ['sass'],
            },
            javascript: {
                files: ['assets/js/src/*.js'],
                tasks: ['uglify:scripts'],
            },
        },

    });

    // Load the plugins that provide the tasks.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    
    // Default task(s) - runs when grunt command is used without any arguments.
    grunt.registerTask('default', ['watch', 'uglify', 'sass']);


};
