<?php
/**
 * The footer template for this theme
 */
?>

  </main>

  <footer class="site-footer">
    <div class="container flex flex--h-between">

      <div class="site-footer__site">
        <div class="site-footer__details flex flex--v-center">
          <a class="site-footer__main-logo" href="/">
            <?php include "assets/svgs/logos/main-logo.svg" ?>
          </a>
          <div class="site-footer__dates">
            <p class="fs--19 fc--white fw--600 lh--120">IndabaX</p>
  					<p class="fs--19 fc--white fw--600 lh--120">Southbank Centre</p>
  					<p class="fs--19 fc--white fw--600 lh--120">13<sup>th</sup> - 15<sup>th</sup> September 2019</p>
  				</div>
        </div>
        <ul class="socials flex flex--inline">
          <li class="socials__item">
            <a class="socials__link" href="#" target="_blank">
              <?php include "assets/svgs/socials/facebook.svg" ?>
            </a>
          </li>
          <li class="socials__item">
            <a class="socials__link" href="#" target="_blank">
              <?php include "assets/svgs/socials/twitter.svg" ?>
            </a>
          </li>
          <li class="socials__item">
            <a class="socials__link" href="#" target="_blank">
              <?php include "assets/svgs/socials/instagram.svg" ?>
            </a>
          </li>
        </ul>
      </div>

      <div class="site-footer__supported align--center">

        <div class="site-footer__support">
          <p class="fs--16 fc--white">In support of</p>
          <img class="site-footer__kofi pos--rel" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/kofi-annan.png' ?>" alt="Kofi Annan Foundation Logo" />
        </div>

        <div class="site-footer__produced">
          <p class="fs--16 fc--white">Produced by</p>
          <ul class="site-footer__logo-items flex flex--v-center flex--h-between">
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/africa.png' ?>" alt="Afr1ca Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/event-horizon.png' ?>" alt="Event Horizon Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/goodvibe.png' ?>" alt="The Good Vibe Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/kush.png' ?>" alt="Kinship Of Kush Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/live-nation.png' ?>" alt="Live Nation Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/letters-live.png' ?>" alt="Letters Live Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/southbank.png' ?>" alt="Salter Brothers Logo" />
            </li>
            <li class="site-footer__logo-item">
            <img class="site-footer__logo" src="<?php echo trailingslashit( get_template_directory_uri() ) . 'assets/images/supporters/sb.png' ?>" alt="Salter Brothers Logo" />
            </li>
          </ul>
        </div>

      </div>

      <div class="site-footer__bottom align--center">
        <ul class="site-footer__bottom-items flex flex--h-center">
          <li class="site-footer__bottom-item">
            <a class="site-footer__bottom-link fs--16 fc--white" href="#">Terms & Conditions</a>
          </li>
          <li class="site-footer__bottom-item">
            <a class="site-footer__bottom-link fs--16 fc--white" href="#">Privacy Policy</a>
          </li>
          <li class="site-footer__bottom-item">
            <a class="site-footer__bottom-link fs--16 fc--white" href="#">Cookie Policy</a>
          </li>
        </ul>
        <p class="fs--16 fc--white">Designed and built by <a class="fc--white" href="https://www.mustardmedia.net/" target="_blank">Mustard Media</a></p>
      </div>

    </div>
  </footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
