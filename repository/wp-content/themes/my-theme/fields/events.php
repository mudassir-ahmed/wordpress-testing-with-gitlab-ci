<?php
/**
 * This is the fields file for the events custom post type.
 */

function event_meta_box()
{
    add_meta_box(
        'event_fields',
        __( 'Event Details', 'eventfields_textdomain' ),
        'event_fields_callback',
        'events'
    );
} // event_meta_box

add_action( 'add_meta_boxes', 'event_meta_box');

function event_fields_callback( $post )
{

  wp_nonce_field( 'event_fields', 'event_fields_nonce' );

  $eventDate = get_post_field_for_metabox('event_date', 'date', 'Date', $post->ID);

  $eventStartTime = get_post_field_for_metabox('event_startTime', 'time', 'Start Time', $post->ID);

  $eventVenue = get_related_post_field_for_metabox('event_venue', 'select', 'Venue', 'venues', $post->ID);

  $eventBuyTicketUrl = get_post_field_for_metabox('event_buyTicketUrl', 'text', 'Buy Ticket URL', $post->ID);

  $eventDescription = get_post_field_for_metabox('event_description', 'textarea', 'Description', $post->ID);

  echo '<div class="custom-admin-fields">';
  echo $eventDate;
  echo $eventStartTime;
  echo $eventVenue;
  echo $eventBuyTicketUrl;
  echo $eventDescription;
  echo '</div>';
  echo '<br clear="all" />';

} // event_fields_callback

function event_fields_save( $post_id )
{
    // Check if our nonce is set.
    if ( ! isset( $_POST['event_fields_nonce'] ) )
    {
        return;
    } // if

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['event_fields_nonce'], 'event_fields' ) ) 
    {
        return;
    } // if

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    {
        return;
    } // if

    // Get values from inputs.
    $eventDate         = $_POST['event_date'];
    $eventStartTime    = $_POST['event_startTime'];
    $eventVenue        = $_POST['event_venue'];
    $eventBuyTicketUrl = $_POST['event_buyTicketUrl'];
    $eventDescription  = $_POST['event_description'];

    // Update the above values in the database.
    update_post_meta($post_id, '_event_date', $eventDate);
    update_post_meta($post_id, '_event_startTime', $eventStartTime);
    update_post_meta($post_id, '_event_venue', $eventVenue);
    update_post_meta($post_id, '_event_buyTicketUrl', $eventBuyTicketUrl);
    update_post_meta($post_id, '_event_description', $eventDescription);

} // event_fields_save

// On save, handle the fields for this post type.
add_action( 'save_post', 'event_fields_save' );


