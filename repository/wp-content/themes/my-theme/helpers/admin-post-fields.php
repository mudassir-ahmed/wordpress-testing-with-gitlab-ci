<?php

function get_post_field_for_metabox($name, $type, $label, $postId = null, $options = array()){

  $currentValue = get_post_meta($postId, '_' . $name, true);

  if(unserialize($currentValue) !== false){

    $currentValue = unserialize($currentValue);

  }

  ob_start();

  echo '<p><label for="' . $name  . '">' . $label . '</label></p>';

  switch($type){
    case 'date':

        echo '<input id="' . $name  . '"  type="date" name="' . $name . '" value="' . $currentValue . '">';

    break;
    case 'time':

        echo '<input id="' . $name  . '"  type="time" name="' . $name . '" value="' . $currentValue . '">';

    break;
    case 'location':

        echo '<input id="' . $name  . '"  type="text" placeholder="long,lat" name="' . $name . '" value="' . $currentValue . '">';

        if ( !empty( $currentValue ) )
        {
            echo '<div>';
            echo '<a href="http://maps.google.com/maps?q=' . $currentValue . '" target="_blank">Open in google maps</a>';
            echo '<p class="description">Update page before using link</p>';
            echo '</div>';
        }

    break;
    case 'text':

      echo '<input id="' . $name  . '"  name="' . $name . '" value="' . $currentValue . '" />';

    break;
    case 'textarea':

      echo '<textarea id="' . $name  . '" name="' . $name . '" style="width:100%;height:200px;">' . $currentValue . '</textarea>';

    break;
    case 'wysiwyg':

      wp_editor( $currentValue, $name);

    break;
    case 'tickbox':

      echo '<input id="' . $name  . '"  type="checkbox" name="' . $name . '" value="1" ' . ($currentValue ? 'checked' : '') . ' />';

    break;
    case 'select':
        
      echo '<select id="' . $name  . '" name="' . $name . '">';

      foreach($options as $val=>$name){
        echo '<option value="' . $val . '" ' . ($val == $currentValue ? 'selected' : '') . '>' . $name . '</option>';
      }

      echo '</select>';

    break;
    case 'image':

      echo '<div id="' . $name . '-preview" style="width:50%;height:150px;display:block;background:#CCC url(' . $currentValue . ') no-repeat;background-size:cover;"></div>';
      echo '<input id="' . $name  . '"  id="' . $name . '-image" name="' . $name . '" value="' . $currentValue . '" /><a href="#" id="' . $name . '-button">Select Image</a> | <a href="#" id="' . $name . '-clear">Clear</a>';

      echo "<script type='text/javascript'>jQuery(document).ready( function($) {

          var button = jQuery('#" . $name . "-button');
          var preview = jQuery('#" . $name . "-preview');
          var field = jQuery('#" . $name . "-image');
          var clear = jQuery('#" . $name . "-clear');
          var file_frame_" . $name . ";

          file_frame_" . $name . " = wp.media.frames.file_frame" . $name . " = wp.media({
            title: $( this ).data( 'File upload' ),
            button: {
              text: $( this ).data( 'Upload' ),
            },
            multiple: false  // Set to true to allow multiple files to be selected
          });

          button.on('click', function() {

            file_frame_" . $name . ".on( 'select', function() {

              // We set multiple to false so only get one image from the uploader
              attachment = file_frame_" . $name . ".state().get('selection').first().toJSON();
              field.val(attachment.url);
              preview.css({
                'background-image': 'url(' + attachment.url + ')',
                'background-size' : 'cover',
                'background-repeat' : 'no-repeat'
              });

            });

            file_frame_" . $name . ".open();

            return false;

          }); // End on click

          clear.on('click', function(e){

            e.preventDefault();

            field.val('');
            preview.css({
              'background-image': 'none',
              'background-size' : 'cover',
              'background-repeat' : 'no-repeat'
            });

          })
    });</script>";


    break;

  }

  echo '<br />';

  $content = ob_get_contents();

  ob_end_clean();

  return $content;

}

function get_related_post_field_for_metabox($name, $type, $label, $relatedPostType, $postId = null){

 // $currentRelated = unserialize(get_post_meta($postId, '_' . $name, true));

  // WARNING: not using unserialize!!!!!
  // NEEDS FIXING - change as a quick fix during development only.
  $currentRelated = get_post_meta($postId, '_' . $name, false);

  if(empty($currentRelated)){
      $currentRelated = array();
  }

  $relatedContent = new WP_Query(
      array(
        'post_type'=>$relatedPostType,
        'posts_per_page'=>99
      )
  );

  ob_start();

  echo '<label style="display:block;">' . $label . '</label>';

  if ( $relatedContent->have_posts() ) {

      echo '<div style="width:43%;margin:0 2% 2%;display:block;float:left;">';
      switch($type){
        case 'radio':
          while ( $relatedContent->have_posts() ) {

              $relatedContent->the_post();
              echo '<input id="' . $name  . '"  name="' . $name . '[]" type="checkbox" value="' . get_the_ID() . '" ' . (in_array(get_the_ID(), $currentRelated) ? 'checked' : '') . ' /><span style="font-size:11px;">' . get_the_title() . '</span><br>';

          }
        break;
        case 'select':

          echo '<select id="' . $name  . '" name="' . $name . '">';
          echo '<option value="">None</option>';
          while ( $relatedContent->have_posts() ) {

              $relatedContent->the_post();
              echo '<option value="' . get_the_ID() .'" ' . (in_array(get_the_ID(), $currentRelated) ? 'selected' : '') . '>' . get_the_title() . '</option>';

          }
          echo '</select>';

        break;
      }

      echo '</div>';
  }

  wp_reset_query();
  $content = ob_get_contents();

  ob_end_clean();

  return $content;

}

function updateRelatedWorkSectors($sector, $selectedWork = array()){

  $workItems = new WP_Query(
      array(
        'post_type'=>'work',
        'posts_per_page'=>99
      )
  );

  if ( $workItems->have_posts() ) {


      while ( $workItems->have_posts() ) {

          $workItems->the_post();

          $currentSectors = unserialize(get_post_meta(get_the_ID(), '_sectors', true));

          // This work item should be in this sector
          if(in_array(get_the_ID(), $selectedWork)){

            if(!in_array($sector, $currentSectors)){

              $currentSectors[] = $sector;

              update_post_meta(get_the_ID(), '_sectors', serialize($currentSectors));

            }



          }else{ // This work item shouldn't be in this sector

            if($currentSectors){
              $keys = array_keys($currentSectors, $sector);
              if (!empty($keys)) {
                  foreach($keys as $key){
                    unset($currentSectors[$key]);
                  }
              }
            }

            update_post_meta(get_the_ID(), '_sectors', serialize($currentSectors));

          }


      }

  }

  wp_reset_query();

}

function updateRelatedWorkServices($work, $selectedServices = array()){

  $serviceItems = new WP_Query(
      array(
        'post_type'=>'service',
        'posts_per_page'=>99
      )
  );

  if ( $serviceItems->have_posts() ) {


      while ( $serviceItems->have_posts() ) {

          $serviceItems->the_post();

          $currentWork = unserialize(get_post_meta(get_the_ID(), '_work', true));

          if(!is_array($currentWork)){

            $currentWork = array();

          }

          // This work item should be in this sector
          if(in_array(get_the_ID(), $selectedServices)){

            if(!in_array($work, $currentWork)){

              $currentWork[] = $work;

              update_post_meta(get_the_ID(), '_work', serialize($currentWork));

            }



          }else{ // This work item shouldn't be in this sector

            if (($key = array_search($work, $currentWork)) !== false) {
                unset($currentWork[$key]);
            }

            update_post_meta(get_the_ID(), '_work', serialize($currentWork));

          }


      }


  }

  wp_reset_query();

}
