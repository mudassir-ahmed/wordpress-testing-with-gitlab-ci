<?php
/**
 * Archive template for event custom post type.
 */

get_header();
?>
<div class="barba-container" data-namespace="buy-tickets">
  <main class="site-content site-content--bg-blue">


    <section class="ticket-list">
      <div class="container">

        <div class="ticket-list__title flex pos--rel z--1">
          <h1 class="ticket-list__header ls--t60 fc--yellow">Buy Tickets</h1>
          <div class="ticket-list__pattern pos--abs z--1"></div>
        </div>

      </div>

      <div class="ticket-list__events">

        <!-- WordPress loop beings -->

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
         
            <?php

                $date = get_post_meta(get_the_ID(), '_event_date', true);

                $startTime = get_post_meta(get_the_ID(), '_event_startTime', true);
                
                $venue = get_post_meta(get_the_ID(), '_event_venue', true);
                
                $buyTicketUrl = get_post_meta(get_the_ID(), '_event_buyTicketUrl', true);

                $description = get_post_meta(get_the_ID(), '_event_description', true);

                $thumbnailUrl = get_the_post_thumbnail_url();
                
            ?>

        <div class="ticket-list__event">
          <div class="container pos--rel">
            <a class="ticket-list__img-holder" href="#">
            <div class="ticket-list__img" style="background-image:url('<?php echo $thumbnailUrl; ?>')"></div>
            </a>
            <div class="ticket-list__details no-edge-margin">
            <h2 class="ticket-list__event-title fs--40 ls--t30"><a class="fc--yellow" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              <div class="ticket-list__divider"></div>
              <div class="ticket-list__times flex">
              <p class="fs--30 fc--white ls--t60 lh--130">Date: <strong><?php echo $date; ?></strong></p>
                <p class="fs--30 fc--white ls--t60 lh--130">Time: <strong><?php echo $startTime; ?></strong></p>
                <p class="fs--30 fc--white ls--t60 lh--130">Venue: <strong><?php echo $venue; ?></strong></p>
              </div>
              <a class="ticket-list__btn fs--30 fc--blue ls--t60 pos--abs" href="<?php echo $buyTicketUrl; ?>"><strong>Buy Tickets</strong></a>
            </div>
            <p class="ticket-list__info bodyfont fc--white lh--130"><?php echo $description; ?></p>
          </div>
        </div><!-- /.ticket-list__event -->

        <?php endwhile; else: ?>
            <?php _e( 'Sorry, no posts matched your criteria.', 'textdomain' ); ?>
        <?php endif; ?>

      <!-- WordPress loop ends -->

      </div><!-- /.ticket-list__events -->

    </section>

<?php get_footer(); ?>
