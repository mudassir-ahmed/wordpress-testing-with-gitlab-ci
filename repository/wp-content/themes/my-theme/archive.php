<?php
/**
 * Default Archive Template
 */

get_header();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php
    if ( has_post_thumbnail() )
    {
        the_post_thumbnail();
    }
    ?>
    <?php the_excerpt(); ?>

<?php endwhile; else: ?>
    <?php _e( 'Sorry, no posts matched your criteria.', 'textdomain' ); ?>
<?php endif; ?>

<?php get_footer(); ?>
