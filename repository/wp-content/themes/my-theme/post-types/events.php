<?php
    
/**
 * Create post type for events which has an archive.
 */
function create_event_post_type()
{
  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' )
      ),
      'public' => true,
      'has_archive' => true,
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_ui' => true,
      'supports' => array(
        'title',
        'page-attributes',
        'thumbnail'
      ),
      'rewrite' => true
    )
  );
} // create_event_post_type

add_action( 'init', 'create_event_post_type' );
