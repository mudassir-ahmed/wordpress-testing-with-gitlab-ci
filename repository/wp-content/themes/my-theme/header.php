<?php
/**
 * The header for this theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- metadata -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- fonts -->
	<link href="https://fonts.googleapis.com/css?family=Oswald:500|Poppins:300,400,500,600,700" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body id="barba-wrapper" <?php body_class(); ?>>

	<header class="site-header pos--fix z--max">
		<div class="container flex flex--r-nowrap flex--v-center pos--rel">

        <a class="site-header__logo" href="<?php echo esc_url( home_url() ); ?>">
				<?php include "assets/svgs/logos/main-logo.svg" ?>
			</a>

			<a class="site-header__hamburger js-class-toggle" href="#" data-class-toggle="open-menu">
				<span class="site-header__hamburger-line"></span>
				<span class="site-header__hamburger-line"></span>
				<span class="site-header__hamburger-line"></span>
			</a>

			<nav class="site-header__holder flex flex--r-nowrap flex--v-center flex--h-between">
				<div class="site-header__dates">
					<p class="fs--19 fc--white fw--600 lh--120">Southbank Centre</p>
					<p class="fs--19 fc--white fw--600 lh--120">13<sup>th</sup> - 15<sup>th</sup> September 2019</p>
				</div>
				<div class="site-header__nav" data-class-toggle="open-menu">
					<a class="site-header__close js-class-remove" href="#" data-class-toggle="open-menu">
						<span class="site-header__close-icon pos--rel"></span>
					</a>

                    <?php

                        include 'MainNavigationWalker.php';
                        wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                                'menu_class' => 'site-header__items flex',
                                'walker' => new MainNavigationWalker()
                            )
                        );
                    ?>

				</div>

                <a class="site-header__cta fs--19 fc--yellow fw--600" href="<?php echo get_post_type_archive_link( 'events' ); ?>">Buy Tickets</a>

            </nav>

		</div>
	</header>

