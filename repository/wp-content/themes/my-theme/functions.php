<?php

/**
 * Theme support
 */
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

/**
 * Include helpers made for this theme.
 */
include 'helpers/admin-post-fields.php';

/**
 * Include post-types made for this theme.
 */
include 'post-types/venues.php';
include 'post-types/programme.php';
include 'post-types/faqs.php';
include 'post-types/vendors.php';
include 'post-types/charities.php';
include 'post-types/events.php';
include 'post-types/news.php';
include 'post-types/speakers.php';

/**
 * Include fields used for this theme.
 */
include 'fields/general.php';
include 'fields/venues.php';
include 'fields/programme.php';
include 'fields/faqs.php';
include 'fields/vendors.php';
include 'fields/charities.php';
include 'fields/events.php';
include 'fields/speakers.php';

/**
 * Enqueue styles and scripts used for this theme.
 */
function indaba_enqueue_style() {
	wp_enqueue_style( 'core',  get_stylesheet_uri(), false );
    wp_enqueue_style( 'main',   trailingslashit( get_template_directory_uri() ) . 'assets/css/main.css', false );
} // indaba_enqueue_style

function indaba_enqueue_script() {
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'modules', trailingslashit( get_template_directory_uri() ) . 'assets/js/modules.min.js', array(), false, true );
    wp_enqueue_script( 'scripts', trailingslashit( get_template_directory_uri() ) . 'assets/js/scripts.min.js', array(), false, true );
} // indaba_enqueue_script

add_action( 'wp_enqueue_scripts', 'indaba_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'indaba_enqueue_script' );

/**
 * Set up custom navigation menus to be used by client.
 * @link https://codex.wordpress.org/Function_Reference/register_nav_menus
 */
add_action( 'after_setup_theme', 'register_custom_nav_menus' );
function register_custom_nav_menus() {
	register_nav_menus( array(
		'main-menu' => 'Primary navigation that appears at the top of the website'
	) );
} // register_custom_nav_menus
