# Run a wordpress installation and run tests
... but I have issues with this - see below.

## Run an install locally
 ```
 git clone git@gitlab.com:mudassir-ahmed/wordpress-testing-with-gitlab-ci.git
 cd wordpress-testing-with-gitlab-ci
 cd dockerstuff && ./start.sh
 ```
 
 ## Then see the output of the curl locally - or see ```screenshots``` folder
 ```
 curl localhost:8000
 ```
 
 # MY ISSUE!
 Why doesn't ```localhost:8000``` on gitlab runner give me the HTML output of the site like it does locally. Please help!